package ru.t1.vlvov.tm.exception.field;

public final class EmailEmptyException extends AbstractFieldNotFoundException {

    public EmailEmptyException() {
        super("Error! Email is empty...");
    }

}