package ru.t1.vlvov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.model.User;

public interface IUserService extends IService<User> {

    @Nullable
    User create(@Nullable String login, @Nullable String password);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User create(@Nullable String login, @Nullable String password, @Nullable Role role);

    @Nullable
    User findByLogin(@Nullable String login);

    @Nullable
    User findByEmail(@Nullable String email);

    @Nullable
    User removeByLogin(@Nullable String login);

    @Nullable
    User removeByEmail(@Nullable String email);

    @Nullable
    User setPassword(@Nullable String id, @Nullable String password);

    @Nullable
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

    boolean isLoginExist(@Nullable String login);

    boolean isEmailExist(@Nullable String email);

    @Nullable
    User lockUserByLogin(@Nullable String id);

    @Nullable
    User unlockUserByLogin(@Nullable String id);

}
